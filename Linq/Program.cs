﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Linq
{
    class Program
    {

        static void Main(string[] args)
        {
            // Task 1: Grel cragir vor@ cuyc kta bolor ayn tver@ voronq 2-i bajanvelis kmna 0 mnacord: 

            // Query Syntax: 

            //int[] arr = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            //var Result = from a in arr where (a % 2) == 0 select a;

            //foreach (var a in Result)
            //{
            //    Console.WriteLine($"{a}");
            //} 


            // Method Syntax: 

            //int[] arr = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };

            //var Result = arr.Where(a => (a % 2 == 0)); 

            //foreach (var a in Result)
            //{
            //    Console.WriteLine($"{a}");
            //} 



            // Task 2: Grel Linq vor@ cuyc kta 1-ic 11 mijakayqum gtnvox bolor tver@:

            // Query Syntax: 

            //int[] K = { -1, 24, 7, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15, 16, 17, 8 };

            //var result = from M in K where M > 1 && M < 11 select M;

            //foreach (int M in result)
            //{
            //    Console.WriteLine($"{M}");
            //} 


            // Method Syntax: 

            //int[] K = { -1, 24, 7, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 15, 16, 17, 8 };

            //var result = K.Where(M => (M > 1 && M < 11));

            //foreach (int item in result) 
            //{
            //    Console.WriteLine($"{item}");
            //} 


            // Task 3: Grel Linq vor@ ktpi tiv@ ev dra qarakusin: 

            //int[] K = { 1, 2, 3, 4, 5, 6 };

            //var Number = from num in K
            //             let SQRNum = num * num
            //             select new { num, SQRNum };

            //foreach (var item in Number)
            //{
            //    Console.WriteLine(item);
            //} 


            // Task 4: Grel cragir vor@ cuyc kta te trvats zangvatsum vor tvic qani hat ka: 

            //int[] arr = { 1, 2, 3, 1, 2, 3, 1, 4, 5, 5, 6, 9, 8, 8, 7, 8, 7, 9, 5, 4, 6 };

            //var n = from x in arr group x by x into y select y;

            //foreach (var item in n)
            //{
            //    Console.WriteLine($"Number {item.Key} appears {item.Count()} times");
            //} 


            // Task 5: 



            //Console.Write("Enter the line please - ");
            //string str = Console.ReadLine(); 

            //var part = from X in str group X by X into y select y;

            //foreach (var item in part)
            //{
            //    Console.WriteLine($"The character is {item.Key} and the count is {item.Count()}");
            //} 


            // Task 6: 

            //string[] str = { "Monday", "Tuesday","Wednesday", "Thursday","Friday","Saturday","Sunday"};

            //var result = from X in str select X;

            //foreach (var item in result)
            //{
            //    Console.WriteLine(item);
            //} 


            // Task 7: 

            //int[] arr = { 1, 2, 3, 1, 2, 3, 1, 2, 3, 4, 5, 6, 8, 7, 9, 7, 98, 7, 9, 7, 9 };

            //var result = from X in arr group X by X into y select y;

            //foreach (var item in result)
            //{
            //    Console.WriteLine($"Number {item.Key} Frequency * Number {item.Key * item.Count()} Frequency {item.Count()}"); 
            //} 

            // Task 8: 


            //string start = "";
            //string end = "";
            //char ch;

            //string[] cities = { "Yerevan", "Moscow", "Berlin", "Amsterdam", "Hamburg", "Myunghen", "Sochi", "Stockholm" };


            //Console.Write("Please enter the starting character from the city - ");
            //ch = (char)Console.Read();
            //start = ch.ToString();

            //Console.WriteLine("Please enter the ending character from the city - ");
            //ch = (char)Console.Read(); 
            //end = ch.ToString();

            //var result = from x in cities 
            //             where x.StartsWith(start) 
            //             where x.EndsWith(end) select x;

            //foreach (var city in result) 
            //{
            //    Console.WriteLine($"The city name starts with {start} ends with {end} characters and the city is {city}"); 
            //}

            // Task 9: Grel Linq vor@ ktpi tveri cucak ev kyelqagri miayn 80-ic mec tver@: 


            //int[] arr = { 34, 534, 67, 8, 9, 567, 332, 5566, 87890, 987865 };

            //Console.Write("The list of the numbers is - ");     

            //foreach (var item in arr)
            //{
            //    Console.Write(item + " ");
            //}

            //var result = from x in arr where x > 80 select x;

            //Console.WriteLine();


            //Console.Write("The all numbers greater then 80 - "); 

            //foreach (var item in result)
            //{
            //    Console.Write(item + "  ");
            //}
            //Console.WriteLine();
            //Console.WriteLine();
            //Console.WriteLine("Thank you for using this app!"); 

            // Task 11: tpel list-i tarrer@ miyevnuyn jamanak cuyc talov user-i uzac tarreri qanakov tarrer@ nvazman kargov:  

            //List<int> list = new List<int>();

            //list.Add(0);
            //list.Add(1); 
            //list.Add(2);
            //list.Add(3);
            //list.Add(4);
            //list.Add(5);
            //list.Add(6);
            //list.Add(7);
            //list.Add(8); 

            //foreach (var item in list)
            //{
            //    Console.WriteLine(item + "  ");
            //}

            //Console.WriteLine("How many records you want to show? ");
            //int n = int.Parse(Console.ReadLine());

            ////list.Sort(); 
            //list.Reverse();

            //Console.WriteLine($"The top {n} records from the list are: ");
            //foreach (var item in list.Take(n))
            //{
            //    Console.WriteLine(item + "  "); 
            //}



            // Task 12: 





            int[] arr = { 1, 2, 24,3, 4, 5, 6, 7, 8, 9 };

            var Result = from x in arr let Sql= x*x select new { x, Sql };

            foreach (var item in Result)
            {
                Console.WriteLine(item); 
            }
        }
    }
}
